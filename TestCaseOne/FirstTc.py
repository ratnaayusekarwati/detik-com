import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException

driver = webdriver.Chrome(executable_path="/Users/oktagongu/browserdriver/chromedriver")
driver.delete_all_cookies()
driver.get("https://www.detik.com/")
driver.maximize_window()

wait = WebDriverWait(driver, 10)
wait.until(EC.element_to_be_clickable((By.LINK_TEXT, "Masuk"))).click()

driver.find_element(By.TAG_NAME, "body").send_keys(Keys.COMMAND + 't')
driver.get('https://connect.detik.com/auth/login/?clientId=64&redirectUrl=https%3A%2F%2Fwww.detik.com%2F&ui=popup&parentURI=https%3A%2F%2Fwww.detik.com%2F')

wait.until(EC.element_to_be_clickable((By.XPATH, "//a[normalize-space()='LOGIN']"))).click()

# Failed login - Phone number
time.sleep(3)
wait.until(EC.element_to_be_clickable((By.XPATH, "//input[@class='uni-input-input']"))).send_keys("08121850728")
wait.until(EC.element_to_be_clickable((By.XPATH, "//uni-button[normalize-space()='Lanjutkan']"))).click()
text = driver.find_element_by_xpath("//uni-view[contains(text(),'Masukkan nomor ponsel yang tepat.')]").text
print(text)

# Success login - Phone number
time.sleep(3)
phone = driver.find_element_by_xpath("//input[@class='uni-input-input']")
phone.send_keys(Keys.SHIFT, Keys.ARROW_UP)
phone.send_keys(Keys.DELETE)
time.sleep(3)
phone.send_keys("8121850728")
# wait.until(EC.element_to_be_clickable((By.XPATH, "//input[@class='uni-input-input']"))).send_keys("8121850728")
wait.until(EC.element_to_be_clickable((By.XPATH, "//uni-button[normalize-space()='Lanjutkan']"))).click()

# Failed login - Password
time.sleep(3)
wait.until(EC.element_to_be_clickable((By.XPATH, "//input[@type='password']"))).send_keys("#Password124")
wait.until(EC.element_to_be_clickable((By.XPATH, "//uni-button[normalize-space()='Log in dengan MPC']"))).click()
verifyLogin = "Nomor ponsel dan password kamu tidak sesuai"
print(verifyLogin)

# Success login - Password
time.sleep(3)
password = driver.find_element_by_xpath("//input[@type='password']")
password.send_keys(Keys.SHIFT, Keys.ARROW_UP)
password.send_keys(Keys.DELETE)
time.sleep(3)
password.send_keys("#Password123")
wait.until(EC.element_to_be_clickable((By.XPATH, "//uni-button[normalize-space()='Log in dengan MPC']"))).click()

# Go to Dashboard
wait.until(EC.element_to_be_clickable((By.ID, "dtkframebar-user"))).click()
wait.until(EC.element_to_be_clickable((By.XPATH, "//a[normalize-space()='Profile Saya']"))).click()
wait.until(EC.element_to_be_clickable((By.LINK_TEXT, "Kelola Akun"))).click()
wait.until(EC.element_to_be_clickable((By.XPATH, "//div[@id='root']//div//div//div//div//div//div//div//form"))).click()

time.sleep(3)
driver.close()
# except NoSuchElementException:
#     print("exception handled")